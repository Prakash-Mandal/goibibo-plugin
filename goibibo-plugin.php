<?php

/**
 * @package goibiboplugin
 *
 * Plugin Name: GoIbibo Travel
 * Plugin URI: https://bitbucket.org/Prakash-Mandal/goibibo-plugin/src
 * Description: Will be used by millions, GoIbibo Travel Plugin is quite possibly the best way in the world to <strong>add travel functionality to your site</strong>. It keeps your site updated with recent data. To get started: activate the GoIbibo plugin and then go to your GoIbibo Developer page to set up your API key.<i class="fi fi-facebook"><i><i class="fi fi-twitter"><i><i class="fi fi-instagram"><i>
 * Version: 1.0.0
 * Author: Prakash Kumar Mandal
 * Author URI:
 * License: GPLv2 or later
 * Text Domain: GoIbibo
*/

/*
 * GoIbibo Travel Plugin - to add search.php hotels, buses and flights functionality to sites.
 * Copyright (C) 2019  Prakash Kumar Mandal

 * This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

if (! defined('ABSPATH') ) {
    defined('ABSPATH') || die('This part cannot be accessed');
}

if (! defined('GOIBIBO_PATH') ) {
    define('GOIBIBO_PATH', plugin_dir_path(__FILE__));
}
if (! defined('GOIBIBO_PATH_URL') ) {
    define('GOIBIBO_PATH_URL', plugin_dir_url(__FILE__));
}
if (! defined('GOIBIBO_DEVELOPER_PATH_URL') ) {
    define('GOIBIBO_DEVELOPER_PATH_URL', 'http://developer.goibibo.com');
}

require_once 'init.php';

if (class_exists('GoIbibo_Plugin')) {
    $goibibo = new GoIbibo_Plugin();
    $goibibo->register();
}

// Activation
$goibibo_activate = new GoIbibo_Plugin_Activate();
register_activation_hook( GOIBIBO_PATH, array( $goibibo_activate, 'activate'));

// Initiatlizing shortcode
add_action('init', array('GoIbiboResultPage' , 'goibibo_plugin_shortcode_init'));

// Deactivation
$goibibo_deactivate = new GoIbiboPluginDeactivate();
register_activation_hook( GOIBIBO_PATH, array( $goibibo_deactivate, 'deactivate'));

// Uninstall
register_uninstall_hook( GOIBIBO_PATH . 'uninstall.php', 'uninstall');
