<?php
/**
 * Created by PhpStorm.
 * User: prakash
 * Date: 27/3/19
 * Time: 6:46 PM
 */

require_once GOIBIBO_PATH . 'inc/goibibo-plugin-activate.php';
require_once GOIBIBO_PATH . 'inc/goibibo-plugin-deactivate.php';

require_once GOIBIBO_PATH . 'uninstall.php';

require_once plugin_dir_path(__FILE__) . 'inc/class.goibibo_plugin.php';
require_once plugin_dir_path(__FILE__) . 'inc/goibibo-option.php';
require_once plugin_dir_path(__FILE__) . 'inc/widgets/class.goibibo-widget.php';
require_once plugin_dir_path(__FILE__) . 'inc/widgets/class.goibibo-bus-widget.php';
require_once plugin_dir_path(__FILE__) . 'inc/widgets/class.goibibo-flight-widget.php';
require_once plugin_dir_path(__FILE__) . 'inc/widgets/class.goibibo-hotel-widget.php';
require_once plugin_dir_path(__FILE__) . 'inc/classes/class.goibibo-result-page.php';
require_once plugin_dir_path(__FILE__) . 'inc/classes/class.goibibo.php';
require_once plugin_dir_path(__FILE__) . 'inc/classes/class.goibibo-bus.php';
require_once plugin_dir_path(__FILE__) . 'inc/classes/class.goibibo-flight.php';
require_once plugin_dir_path(__FILE__) . 'inc/classes/class.goibibo-hotel.php';
