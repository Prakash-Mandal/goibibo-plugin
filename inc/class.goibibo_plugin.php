<?php 
/**
 * @package goibibo-plugin
 * GoIbibo Class for calling Scripts, Styles, Hooks and Filters
 */

if (! class_exists('GoIbibo_Plugin') ) {
    /**
     * Class for plugin initialization
     */
    class GoIbibo_Plugin
    {
        /**
         * Register Function
         */
        public function register() 
        {
            add_action( 'wp_enqueue_scripts', array( $this, 'enqueue' ) );

            add_action( 'wp_ajax_SeatLayout', array( $this, 'calling_ajax') );

            add_action( 'wp_ajax_nopriv_SeatLayout', array( $this, 'calling_ajax') );

        }

        /**
         * Enqueing Scripts And Styles
         */
        public function enqueue() 
        {
            wp_enqueue_style( 'bootstrap', GOIBIBO_PATH_URL . 'assets/bootstrap.min.css');
            wp_enqueue_style( 'gostyle', GOIBIBO_PATH_URL . 'assets/go_style.css');
            wp_enqueue_style( 'ion_slider', 'https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/css/ion.rangeSlider.min.css' );
            wp_enqueue_style( 'rating_css', "https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" );
            wp_enqueue_script( 'jquery');
            wp_register_script( 'jQuery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js' );
            wp_enqueue_script( 'bootstrap_JScript', GOIBIBO_PATH_URL . 'assets/bootstrap.min.js', 'jquery');
            wp_enqueue_script( 'popper_JS', "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js", 'jquery');
            wp_enqueue_script( 'goibibo_widget', GOIBIBO_PATH_URL . 'assets/goibibo_widget.js', 'jquery' );
            wp_register_script( 'goBus_JScript', GOIBIBO_PATH_URL . 'assets/gobus_script.js', 'jquery' );
            wp_register_script( 'ion_slider_script', 'https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/js/ion.rangeSlider.min.js' );
            
            wp_localize_script( 
                'goBus_JScript', 
                'seatscript',  
                array( 'ajax_url' =>  admin_url( 'admin-ajax.php' )
                )
            );
        }

        /**
         * calling_ajax
         *
         * @return void
         */
        public function calling_ajax()
        {
            $i = $_POST['i'];
            $skey = $_POST['skey'];

            $seats = new GoIbibo_Bus();

            $seats->get_bus_seat_layout($i, $skey);
        }

    }
}