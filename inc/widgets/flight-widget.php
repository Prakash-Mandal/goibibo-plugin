<?php
/**
 * @package goibibo-plugin
 * 
 */

echo '</div><div id="flight-search" 
    class="collapse widget-text wp_widget_plugin_box">';
    echo '<form class="form-group" method="POST" 
            action="'.get_permalink(get_page_by_title('Search Results')).'">';
        // Display source field
            echo '<div class="form-group">
                        <label for="' . esc_attr($this->get_field_id('source')) . '">' .
                __('Source:', 'text_domain') . '</label>';?>
            <select name="<?php echo $this->get_field_name('source'); ?>" 
            id="<?php esc_attr($this->get_field_id('source')); ?>" class="form-control">
            <?php
            // Loop through options and add each one to the select dropdown
            foreach ($this->options as $key => $name) {
                echo '<option value="' . esc_attr($key) . '" id="' . 
                esc_attr($key) . '" ' . selected($source, $key, false) . '>' . 
                $name . '</option>';
            } ?>
            </select></div><?php
        // Display destination field
            echo '<div class="form-group">
                        <label for="' . esc_attr($this->get_field_id('destination')) . '">' .
                __('Destination:', 'text_domain') . '</label>'; ?>
            <select name="<?php echo $this->get_field_name('destination'); ?>" 
            id="<?php esc_attr($this->get_field_id('destination')) ?>" class="form-control">
            <?php
            // Loop through options and add each one to the select dropdown
            foreach ($this->options as $key => $name) {
                echo '<option value="' . esc_attr($key) . '" id="' . 
                esc_attr($key) . '" ' . selected($destination, $key, false) . '>' . 
                $name . '</option>';
            } ?>
            </select></div><?php
        // Display trvl_date field
            echo '<div class="form-group">
                    <label for="' . esc_attr($this->get_field_id('flight_date')) .'">' .
                    __('Travel Date:', 'text_domain') . '</label>' .
                '<input class="form-control" id="' . esc_attr($this->get_field_id('flight_date')) . 
                '"name="' . esc_attr($this->get_field_name('flight_date')) . '"
                        type="date" value="' . date("Y-m-d") . '" min="' . date("Y-m-d") . '" />
                    </div>';

        // Display seating class option
            $options = array(
                'E' => __('Economy', 'text_domain'),
                'B' => __('Business', 'text_domain'),
            );
            echo '<div class="form-group">
                        <label for="' . esc_attr($this->get_field_id('seating_class')) . '">' .
                __('Seating Class :', 'text_domain') . '</label>' .
                '<select name="' . $this->get_field_name('seating_class') . '"
                        id="' . esc_attr($this->get_field_id('seating_class')) . '" class="form-control">';
            // Loop through options and add each one to the select dropdown
            foreach ($options as $key => $name) {
                echo '<option value="' . esc_attr($key) . '" 
                            id="' . esc_attr($key) . '" ' . selected($seating_class, $key, false) . '>' .
                    $name . '</option>';
            }
            echo '</select>
                    </div>';
        // Display Number of Adults option
            // Your options array
            $options = array(1, 2, 3, 4, 5, 6, 7, 8, 9);
            echo '<div class="row"><div class="col-4 form-group">
                        <label for="' . esc_attr($this->get_field_id('adults')) . '">' .
                __('Number of adults :', 'text_domain') . '</label>' .
                '<select name="' . $this->get_field_name('adults') . '"
                        id="' . esc_attr($this->get_field_id('adults')) . '" class="form-control">';
            // Loop through options and add each one to the select dropdown
            foreach ($options as $key => $name) {
                echo '<option value="' . esc_attr($name) . '" 
                            id="adults' . esc_attr($name) . '" ' . selected($adults, $key, false) . '>' .
                    $name . '</option>';
            }
            echo '</select>
                    </div>';
        // Display Number of child(s) option
            // Your options array
            $options = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            echo '<div class="col-4 form-group">
                        <label for="' . esc_attr($this->get_field_id('children')) . '">' .
                __('Number of Child :', 'text_domain') . '</label>' .
                '<select name="' . $this->get_field_name('children') . '"
                        id="' . esc_attr($this->get_field_id('children')) . '" class="form-control">';
            // Loop through options and add each one to the select dropdown
            foreach ($options as $key => $name) {
                echo '<option value="' . esc_attr($name) . '" 
                            id="child' . esc_attr($name) . '" ' . selected($children, $key, false) . '>' .
                    $name . '</option>';
            }
            echo '</select>
                    </div>';
        // Display Number of infant(s) option
            // Your options array
            $options = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
            echo '<div class="col-4 form-group">
                        <label for="' . esc_attr($this->get_field_id('infants')) . '">' .
                __('Number of Infant :', 'text_domain') . '</label>' .
                '<select name="' . $this->get_field_name('infants') . '"
                        id="' . esc_attr($this->get_field_id('infants')) . '" class="form-control">';
            // Loop through options and add each one to the select dropdown
            foreach ($options as $key => $name) {
                echo '<option value="' . esc_attr($name) . '" 
                            id="infant' . esc_attr($name) . '" ' . selected($infants, $key, false) . '>' .
                    $name . '</option>';
            }
            echo '</select>
                    </div> <div>';
        echo '<input class="btn" type="submit" 
                name="flight-search" value="Flight Search">';
        echo '</form></div>';