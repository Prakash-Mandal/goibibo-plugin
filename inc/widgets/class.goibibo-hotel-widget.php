<?php
/**
 * @package goibibo-plugin
 *
 * Hotel Widget
 */

 if (! class_exists('GoIbibo_Hotel_Widget') ) {
    /**
     * 
    */
    class GoIbibo_Hotel_Widget extends WP_Widget
    {
        /**
         * To store City List
          */
        private $_options;

        // Main constructor
        /**
         * GoIbibo_Hotel_Widget constructor.
         */
        public function __construct()
        {
            load_plugin_textdomain('goibibo-plugin');

            $this->_get_city_list();

            parent::__construct(
                'goibibo_hotel_widget',
                __('GoIbibo Hotel Widget', 'goibibo-plugin'),
                array(
                    'description' => __('Widget for Hotel Search', 'goibibo-plugin'),
                    'customize_selective_refresh' => true,
                )
            );
        }

        /**
         * Get City List
         */
        private function _get_city_list()
        {
            global $wpdb;
            global $table_prefix;
            $results = $wpdb->get_results(
                "SELECT city_name,city_ID FROM ".DB_NAME.'.'.$table_prefix."cities  
                ORDER BY `city_name` ASC "
            );

            $this->_option = ['' => __('Select', 'text_domain')];
            foreach ($results as $key) {
                $this->_options[__($key->city_ID, 'text_domain')]
                    = __($key->city_name, 'text_domain');
            }
        }

        /**
         * The widget form (for the backend )
         *
         */
        public function form($instance)
        {
            // Set widget defaults
            $defaults = array(
                'title' => 'Hotel Search',
                'city_name' => 'Bhubaneswar',
                'check_in_date' => date('Y-m-d'),
                'check_out_date'   => date("Y-m-d", time() + 86400),
            );

            // Parse current settings with defaults
            wp_parse_args(( array )$instance, $defaults);
            
            $title = isset($instance['title']) ?
                wp_strip_all_tags($instance['title']) : $defaults['title'];
            $city_name    = isset($instance['city_name']) ?
                wp_strip_all_tags($instance['city_name']) : $defaults['city_name'];
            $title    = isset($instance['title']) ?
                wp_strip_all_tags($instance['title']) : $defaults['title']; 
                
            ?>
            <div class="form-group">
                <div class="form-group">
                    <label for="<?php echo esc_attr($this->get_field_id('title')); ?>">
                        <?php _e('Widget Title : ', 'text_domain'); ?>
                    </label>
                    <input class="form-control" 
                    id="<?php echo esc_attr($this->get_field_id('title')); ?>" 
                    name="<?php echo esc_attr($this->get_field_name('title')); ?>" 
                    type="text" value="<?php echo esc_attr($title); ?>" >
                </div>

                <div class="form-group">
                    <label for="<?php echo esc_attr($this->get_field_id('city_name')); ?>" >
                        <?php echo __('Destination:', 'text_domain'); ?>
                    </label>
                    <select id="<?php echo esc_attr($this->get_field_id('city_name')); ?>" 
                    name="<?php echo esc_attr($this->get_field_name('city_name')); ?>" 
                    value="<?php echo esc_attr($city_name); ?>">
                    <?php
                    foreach ($this->_options as $key => $value) {
                        echo '<option value="' . $key . '"
                            id="' . $value . '" ' .
                            selected($city_name, $value, false) . '>' . $value . '</option>';
                    } ?></select>
                </div>
            </div>
        <?php
        }
        
        /**
         * Update widget settings
         *
         * @param  mixed $new_instance
         * @param  mixed $old_instance
         *
         * @return void
         */
        public function update($new_instance, $old_instance)
        {
            $instance = $old_instance;
            $instance['title']    = isset($new_instance['title']) ? 
                wp_strip_all_tags($new_instance['title']) : 'Hotel Search';
            $instance['city_name'] = isset($new_instance['city_name']) ? 
                wp_strip_all_tags($new_instance['city_name']) : 'bhubaneswar';
            $instance['check_in_date'] = isset($new_instance['check_in_date']) ? 
                wp_strip_all_tags('check_in_date')  : date('Y-m-d');
            $instance['check_out_date']   = isset($new_instance['check_out_date']) ? 
                wp_strip_all_tags('check_out_date')  : date("Y-m-d", time() + 86400);
            return $instance;
        }

        /**
         * Display the widget
         */
        public function widget($args, $instance)
        {
            // Check the widget options
            $title = isset($instance['title']) ?
                apply_filters('widget_title', $instance['title']) : 'Hotel Search';
            $city_name = isset($instance['city_name']) ?
                $instance['city_name'] : 'Bhubaneswar';

            // WordPress core before_widget hook (always include )
            echo $args['before_widget'];
            // Display the widget
            echo '<div class="wp-widget collapsed" data-toggle="collapse" data-target="#hotel-search">';
            // Display widget title if defined
            if ($title) {
                echo $args['before_title'] . $title . $args['after_title'];
            }
            echo '</div><div id="hotel-search" class="collapse widget-text wp_widget_plugin_box">';


            echo '<form class="form-group" method="POST" action="'.
                get_permalink(get_page_by_title('Search Results')).'">';
            // Display city _name field
            echo '<div class="form-group">
                    <label for="' . esc_attr($this->get_field_id('city_name')) . '">' .
                __('City :', 'text_domain') . '</label>'; ?>
            <label for="<?php echo esc_attr($this->get_field_id('city_name')); ?>">
                <?php __('Destination:', 'text_domain'); ?>
            </label>
            <select id="<?php echo esc_attr($this->get_field_id('city_name')); ?>" 
            name="<?php echo esc_attr($this->get_field_name('city_name')); ?>" 
            value="<?php echo esc_attr($city_name); ?>" class="form-control">
            <?php
            foreach ($this->_options as $key => $value) {
                echo '<option value="' . $key . '"
                    id="' . $key . '" ' .
                    selected($city_name, $value, false) . '>' . $value . '</option>';
            } ?></select>
            </div><?php
            // Display something if check_in_date is true

            echo '<div class="form-group ">
                    <label for="' . esc_attr($this->get_field_id('chec_in_date')) . 
                    '">' . _e('Check-In Date:', 'text_domain') . '</label>' .
                '<input class="form-control check-in" id="' .
                    esc_attr($this->get_field_id('check_in_date')) .
                    '" name="' . esc_attr($this->get_field_name('check_in_date')) . '"
                    type="date" value="' . esc_attr(date('Y-m-d')) . '" 
                    min="' . esc_attr(date('Y-m-d')) . '" />
                        </div>';
            // Display something if check_out_date is true
            echo '<div class="form-group">
                    <label for="' . esc_attr($this->get_field_id('check_out_date')) . 
                    '">' . _e('Check-Out Date:', 'text_domain') . '</label>' .
                '<input class="check-out form-control" id="' .
                    esc_attr($this->get_field_id('check_out_date')) .
                    '" name="' . esc_attr($this->get_field_name('check_out_date')) . '"
                    type="date" value="' . esc_attr(date('Y-m-d', time() + 232800)) . 
                    '" min="' . esc_attr(date('Y-m-d')) . '" /></div>';
            echo '<input class="btn" type="submit" name="hotel-search" 
                value="Hotel Search">';
            echo '</form></div>';
            // WordPress core after_widget hook (always include )
            echo $args['after_widget'];
        }
    }
 }