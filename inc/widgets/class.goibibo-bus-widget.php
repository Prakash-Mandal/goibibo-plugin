<?php
/**
 * @package goibibo-plugin
 *
 * Bus Widget
 */

if (! class_exists('GoIbibo_Bus_Widget')) {
    /**
     * GoIbibo Bus Widget
     */
    class GoIbibo_Bus_Widget extends WP_Widget
    {
        /**
         * To store City List
         */
        private $_cityList;

        /**
         * GoIbibo_Bus_Widget constructor.
         */
        public function __construct()
        {
            load_plugin_textdomain('goibibo-plugin');

            parent::__construct(
                'goibibo_bus_widget',
                __('GoIbibo Bus Widget', 'goibibo-plugin'),
                array(
                    'calssname' => 'my-Widget',
                    'description' => __(
                        'Widget for Search Current Buses ',
                        'goibibo-plugin'
                    ),
                    'customize_selective_refresh' => true,
                )
            );

            $this->_get_city_list();
        }

        /**
         * Get City List
         *
         * @return void
         */
        private function _get_city_list()
        {
            $json = file_get_contents(GOIBIBO_PATH_URL . 'data/BusCityList.json');
            $data = json_decode($json);

            $this->_cityList = ['' => __('Select', 'text_domain')];

            foreach ($data->BusCity as $key => $value) {
                $key = ucfirst(strtolower(__($key, 'text_domain')));
                $value = ucfirst(strtolower(__($value, 'text_domain')));
                $this->_cityList[$key] = $value;
            }
        }

        /**
         * @desc The widget form ( for the backend )
         *
        */
        public function form($instance)
        {

            // Set widget defaults
            $defaults = array(
                'title'    => 'Bus Search',
                'source'     => 'Bhubaneswar',
                'destination' => 'Kolkata',
            );

            // Parse current settings with defaults
            wp_parse_args(( array ) $instance, $defaults);

            $title = (isset($instance['title'])) ?
                $instance['title'] : $defaults['title'];
            $source = (isset($instance['source'])) ?
                $instance['source'] : $defaults['source'];
            $destination = (isset($instance['destination'])) ?
                $instance['destination'] : $defaults['destination'];

            // Widget Title?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>">
                    <?php _e('Widget Title', 'text_domain'); ?>
                </label>
                <input class="widefat"
                    id="<?php echo esc_attr($this->get_field_id('title')); ?>"
                    name="<?php echo esc_attr($this->get_field_name('title')); ?>"
                    type="text" value="<?php echo esc_attr($title); ?>" />
            </p>

            <?php // source Field?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('source')); ?>">
                    <?php _e('Source:', 'text_domain'); ?>
                </label>

                <select id="<?php echo esc_attr($this->get_field_id('source')) ?>"
                    name="<?php echo esc_attr($this->get_field_name('source')) ?>"
                    value="<?php echo esc_attr($source) ?>">
                    <?php
                    // Loop through options and add each one to the select dropdown
                    foreach ($this->_cityList as $key => $name) {
                        echo '<option value="' . esc_attr($key) . '" id="' .
                        esc_attr($key) . '" ' . selected($source, $key, true) . '>' .
                        $name . '</option>';
                    } ?>
                </select>
            </p>

            <?php // destination Field?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('destination')); ?>">
                    <?php echo _e('destination:', 'text_domain'); ?>
                </label>
                <select id="<?php echo esc_attr($this->get_field_id('destination')) ?>"
                    name="<?php echo esc_attr($this->get_field_name('destination')) ?>"
                    value="<?php echo esc_attr($destination) ?>">
                    <?php
                    // Loop through options and add each one to the select dropdown
                    foreach ($this->_cityList as $key => $name) {
                        echo '<option value="' . esc_attr($key) . '" id="' .
                        esc_attr($key) . '" ' . selected($destination, $key, false) .
                        '>' . $name . '</option>';
                    } ?>
                </select>
            </p>
        <?php
        }
        /**
         *  Update widget settings
         *
         * @param  mixed $new_instance
         * @param  mixed $old_instance
         *
         * @return void
         */
        public function update($new_instance, $old_instance)
        {
            $instance = $old_instance;
            $instance['title'] = isset($new_instance['title']) ?
            wp_strip_all_tags($new_instance['title']) : $old_instance['title'];
            $instance['source'] = isset($new_instance['source']) ?
            wp_strip_all_tags($new_instance['source']) : $old_instance['source'];
            $instance['destination'] = isset($new_instance['destination']) ?
            wp_strip_all_tags($new_instance['destination']):$old_instance['destination'];
            $instance['bus_travel_date'] = isset($new_instance['bus_travel_date']) ?
            wp_strip_all_tags($new_instance['bus_travel_date'])  :
            $old_instance['bus_travel_date'];
            $instance['bus_return_date'] = isset($new_instance['bus_return_date']) ?
            wp_strip_all_tags($new_instance['bus_return_date'])  :
            $old_instance['bus_return_date'];

            return $instance;
        }
        /**
         * @desc Display the widget
         *
         * @param Array $args
         * @param Array $instance
         *
         * @return void
         */
        public function widget($args, $instance)
        {
            // Check the widget options
            $title = isset($instance['title']) ?
            apply_filters('widget_title', $instance['title']) : 'Bus Widget';
            $source = isset($instance['source']) ?
            $instance['source'] : 'Bhubaneswar';
            $destination = isset($instance['destination']) ?
            $instance['destination'] : 'Kolkata';
            $bus_travel_date = isset($instance['bus_travel_date']) ?
            $instance['bus_travel_date'] : date("Y-m-d");
            $bus_return_date = isset($instance['bus_return_date']) ?
            $instance['bus_return_date'] : date("Y-m-d", time()+172800);

            // WordPress core before_widget hook ( always include )
            echo $args['before_widget'];
            // Display the widget
            echo '<div class="wp-widget collapsed" data-toggle="collapse" 
                data-target="#bus-search">';

            // Display widget title if defined
            if ($title) {
                echo $args['before_title'] . $title . $args['after_title'];
            }

            include_once 'bus-widget.php';

            // WordPress core after_widget hook ( always include )
            echo $args['after_widget'];
        }
    }
}