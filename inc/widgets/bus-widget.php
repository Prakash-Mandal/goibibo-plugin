<?php
/**
 * @package goibibo-plugin
 */
?>
</div><div id="bus-search" class="collapse widget-text wp_widget_plugin_box">
    <form class="form-group" method="POST" action="<?php echo get_permalink(get_page_by_title('Search Results')) ?>">

        <div class="form-group">
            <label for="<?php echo esc_attr($this->get_field_id('source')) ?>">
                <?php _e('Source:', 'text_domain') ?>
            </label>
            <select class="form-control" 
                id="<?php echo esc_attr($this->get_field_id('source')) ?>"
                name="<?php echo esc_attr($this->get_field_name('source')) ?>"
                value="<?php echo esc_attr($source) ?>">
                <?php
                // Loop through options and add each one to the select dropdown
                foreach ($this->_cityList as $key => $name) {
                    echo '<option value="' . esc_attr($key) . '" id="' . 
                    esc_attr($key) . '" ' . selected($source, $key, false) . '>' . 
                    $name . '</option>';
                } ?>
            </select>
        </div>
    <?php
    // Display destination field?>
        <div class="form-group">
                <label 
                    for="<?php echo esc_attr($this->get_field_id('destination')) ?>">
                <?php _e('Destination:', 'text_domain') ?>
                </label>
            <select class="form-control" 
                id="<?php echo esc_attr($this->get_field_id('destination')) ?>"
                name="<?php echo esc_attr($this->get_field_name('destination')) ?>"
                value="<?php echo esc_attr($destination) ?>">
                <?php
                // Loop through options and add each one to the select dropdown
                foreach ($this->_cityList as $key => $name) {
                    echo '<option value="' . esc_attr($key) . '" id="' . 
                    esc_attr($key) . '" ' . selected($destination, $key, false) . '>' . 
                    $name . '</option>';
                } ?>
            </select>
        </div>
    <?php
    // Display if bus_travel_date is true?>
        <div class="form-group">
            <label 
                for="<?php echo esc_attr($this->get_field_id('bus_travel_date')) ?>">
                <?php _e('Travel Date:', 'text_domain') ?>
            </label>
            <input class="form-control" 
                id="<?php echo esc_attr($this->get_field_id('bus_travel_date')) ?>"
                name="<?php echo esc_attr($this->get_field_name('bus_travel_date')) ?>"
                type="date" value="<?php echo date('Y-m-d'); ?>" min="<?php echo date('Y-m-d'); ?>" />
            </div>
    <input class="btn" type="submit" id="bus-search"
        name="bus-search" value="Bus Search">
</form>
</div>