<?php
/**
 * @package goibibo-plugin
 *
 */


if (! class_exists('GoIbibo_Flight_Widget') ) {
     /**
      * GoIbibo Flight Widget
      */
    class GoIbibo_Flight_Widget extends WP_Widget
    {

        /**
         * @var
         */
        private $_options ;
        // Main constructor
        /**
         * GoIbibo_Flight_Widget constructor.
         */
        public function __construct()
        {

            load_plugin_textdomain( 'goibibo-plugin' );

            // Setting timezone for India
            date_default_timezone_set( 'Asia/Kolkata' );

            $this->_get_airport_list();

            parent::__construct( 
                'goibibo_flight_widget',
                __( 'GoIbibo Flight Widget', 'goibibo-plugin' ),
                array( 
                    'description' => __( 'Widget for Flight Search', 'goibibo-plugin' ),
                    'customize_selective_refresh' => true,
                )
            );

        }

        /**
         * @desc Get Airport Code list
         */
        private function _get_airport_list(  )
        {
            $airports = json_decode( 
                file_get_contents( GOIBIBO_PATH_URL.'data/airports.json' ),
                true
            );
            // Your options array
            $this->_options = ['' => __( 'Select', 'text_domain' )];
            foreach ( $airports['data'] as $key => $value ) {
                if ('India' === $value['country'] ) {
                    $select_item = $value['city'] . '( ' . $value['code'] . ' )';
                    $this->_options[ __( $value['code'], 'text_domain' )] = __( $select_item, 'text_domain' );
                }
            }
        }

        /**  
         * @desc The widget form ( for the backend )
         * 
         * @param Widget $instance 
         * 
         */
        public function form( $instance )
        {
            // Set widget defaults
            $defaults = array( 
                'title'    => 'Flight Search',
                'source'     => 'Bhubaneswar(BBI)',
                'destination' => 'Kolkata( CCU )'
            );

            // Parse current settings with defaults
            wp_parse_args( $instance, $defaults );

            $title    = isset( $instance['title'] ) ? 
                wp_strip_all_tags( $instance['title'] ) : $defaults['title'];
            $source     = isset( $instance['source'] ) ? 
                wp_strip_all_tags( $instance['source'] ) : $defaults['source'];
            $destination = isset( $instance['destination'] ) ? 
                wp_strip_all_tags( $instance['destination'] ) : $defaults['destination'];

            ?>
            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
                    <?php echo __( 'Widget Title', 'text_domain' ); ?>
                </label>
                <input class="widefat" 
                    id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" 
                    name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" 
                    type="text" value="<?php echo esc_attr( $title ); ?>" />
            </p>

            <?php 
            ?>
            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'source' ) ); ?>">
                    <?php echo __( 'Source:', 'text_domain' ); ?>
                </label>
                <select name="<?php echo $this->get_field_name( 'source' ); ?>" 
                id="<?php echo $this->get_field_id( 'source' ); ?>" 
                value="<?php echo esc_attr( $source ); ?>" >
                <?php 
                // Loop through options and add each one to the select dropdown
                foreach ( $this->_options as $key => $name ) {
                    echo '<option value="' . esc_attr( $key ) . '" id="' . 
                    esc_attr( $key ) . '" ' . selected( $source, $key, false ) . 
                    '>' . $name . '</option>';
                } ?>
                </select>
            </p>

            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'destination' ) ); 
                    ?>">
                    <?php echo __( 'Destination:', 'text_domain' ); ?>
                </label>
                <select name="<?php echo $this->get_field_name( 'destination' ); ?>" 
                id="<?php echo $this->get_field_id( 'destination' ); ?>" 
                class=form-control" value="<?php echo esc_attr( $source ); ?>" >
                <?php
                // Loop through options and add each one to the select dropdown
                foreach ( $this->_options as $key => $name ) {
                    echo '<option value="' . esc_attr( $key ) . '" id="' . 
                    esc_attr( $key ) . '" ' . selected( $destination, $key, false ) .
                    '>' . $name . '</option>';
                } ?>
                </select>
            </p>

            <?php 
        }
        /**
         * Update widget settings
         * @desc Saving data
         * @param $new_instance Instance Array
         * @param $old_instance Instance Array
         *
         * @return mixed
         */
        public function update( $new_instance, $old_instance )
        {
            $instance = $old_instance;

            $instance['title']    = isset( $new_instance['title'] ) ? 
                wp_strip_all_tags( $new_instance['title'] ) : $old_instance['title'];
            $instance['source']     = isset( $new_instance['source'] ) ? 
                wp_strip_all_tags( $new_instance['source'] ) : $old_instance['source'];
            $instance['destination'] = isset( $new_instance['destination'] ) ? 
                wp_strip_all_tags( $new_instance['destination'] ) : 
                $old_instance['destination'];
            $instance['trvl_date'] = isset( $new_instance['flight_date'] ) ? 
                wp_strip_all_tags( 'trvl_date' )  : $old_instance['flight_date'];
            $instance['arvl_date']   = isset( $new_instance['arrival_date'] ) ? 
                wp_strip_all_tags( 'arvl_date' )  : $old_instance['arrival_date'];
            $instance['seating_class']   = isset( $new_instance['seating_class'] ) ? 
                wp_strip_all_tags( 'seating_class' )  : $old_instance['seating_class'];
            $instance['adults']   = isset( $new_instance['adults'] ) ? 
                wp_strip_all_tags( 'adults' )  : $old_instance['adults'];
            $instance['children']   = isset( $new_instance['children'] ) ? 
                wp_strip_all_tags( 'children' )  : $old_instance['children'];
            $instance['infants']   = isset( $new_instance['infants'] ) ? 
                wp_strip_all_tags( 'infants' )  : $old_instance['infants'];

            return $instance;
        }
        /** 
         * Display the widget
         *
         * @param  mixed $args
         * @param  mixed $instance
         *
         * @return void
         */
        public function widget( $args, $instance )
        {
            // Check the widget options
            $title    = isset( $instance['title'] ) ? 
                apply_filters( 'widget_title', $instance['title'] ):'Flight Search';
            $source     = isset( $instance['source'] ) ? 
                $instance['source'] : 'Bhubaneswar(BBI)';
            $destination = isset( $instance['destination'] ) ? 
                $instance['destination'] : 'Kolkata(CCU)';
            $flight_date = isset( $instance['flight_date'] ) ? 
                $instance['flight_date'] : date('Y-m-d');
            $arrival_date = isset( $instance['arrival_date'] ) ? 
                $instance['arvl_date'] : '';
            $seating_class = isset( $instance['seating_class'] ) ? 
                $instance['seating_class'] : 'E';
            $adults = isset( $instance['adults'] ) ? $instance['adults'] : '1';
            $children = isset( $instance['children'] ) ? $instance['children'] : '0';
            $infants = isset( $instance['infants'] ) ? $instance['infants'] : '0';

            // WordPress core before_widget hook ( always include )
            echo $args['before_widget'];
            // Display the widget
            echo '<div class="wp-widget collapsed" data-toggle="collapse"
                data-target="#flight-search">';

            // Display widget title if defined
            if ( $title ) {
                echo $args['before_title'] . $title . $args['after_title'];
            }
            include_once 'flight-widget.php';
            // WordPress core after_widget hook ( always include )
            echo $args['after_widget'];
        }
    }
}