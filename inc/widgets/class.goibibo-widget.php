<?php
/**
 * @package goibibo-plugin
 *
 */

add_action('widgets_init', function () {
    register_widget('GoIbibo_Hotel_Widget');
    register_widget('GoIbibo_Flight_Widget');
    register_widget('GoIbibo_Bus_Widget');
});
