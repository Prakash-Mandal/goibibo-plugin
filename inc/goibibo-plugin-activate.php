<?php
/**
 * @package goibiboplugin
 *
 */

if (! class_exists('GoIbibo_Plugin_Activate') ) {
    /**
     * Plugin Activation Class
     */
    class GoIbibo_Plugin_Activate
    {

        private $_tableFound;
        private $_tableName;
        /**
         * 
         */
        public function __construct() 
        {
        }

        /**
         * 
         */
        public function activate() 
        {

            $this->_create_city_list();

            // Display alert box on plugin activation.
             add_action('admin_notices', $this->notices($this->_tableFound));


            //flush rewrite rules
            flush_rewrite_rules();
        }

        /**
         * _create_city_list
         *
         * @return void
         */
        private function _create_city_list()
        {

            global $wpdb;
            global $table_prefix;

            $this->_tableName = $table_prefix."cities";
            $this->_tableFound = $wpdb->query(
                'show tables like \''.$this->_tableName.'\''
            );
            
            if (! $this->_tableFound) {

                $create_table = "CREATE TABLE IF NOT EXISTS " . $this->_tableName . ' ( city_name VARCHAR(28) NOT NULL,
                  city_ID VARCHAR(19) NOT NULL, domestic_flag VARCHAR(13) NOT NULL)';

                $wpdb->query($create_table);

                $csv_file = GOIBIBO_PATH . 'data/city_list.csv';
                if (false !== ($handle = fopen($csv_file, "r"))) {
                    while (false !== ($data = fgetcsv($handle, 100))) {
                        $num = count($data);
                        for ($c=0; $c < $num; $c++) {
                            $col[$c] = $data[$c];
                        }

                        if ( 1 === $col[2] ) {
                            $col1 = $col[0];
                            $col2 = $col[1];
                            $col3 = $col[2];

                            //SQL Query to insert data into DataBase
                            $query = "INSERT INTO " . $this->_tableName . " ( city_name, city_id, domestic_flag
                            ) VALUES ('" . $col1 . "','" . $col2 . "','" . $col3 . "')";

                            $results = $wpdb->query($query);
                        }
                    }
                    fclose($handle);
                }
            }

        }

        /**
         * Callback for Plugin Activation notice.
         *
         * @param  mixed $value
         *
         * @return string
         */
        public function notices($value = false) 
        {
            if ($value) { ?>
                <div class="notice updated">
                    <p>
                        Plugin is loading data into <?php echo $this->_tableName; ?> Please wait !!<br>
                        You can set the credentials for the plugin <a >onthis page .</>
                    </p>
                </div> <?php
            } else { ?>
                <div class="notice error">
                    <p>
                        Error in loading data into <?php echo $this->_tableName; ?><br>
                        Please try again !!');
                    </p>
                </div> <?php
            }
        }

        /**
         * Add Search Page to the Site
         *
         * @return void
         */
        public function search_page()
        {
            // Create Search page
            $my_search_page = array(
                'post_title'    => wp_strip_all_tags( 'Search Results Test' ),
                'post_content'  => '<div style="width:100% !important;" 
                    class="container-fluid">[result_shortcode]</div>',
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'     => 'page',
              );
  
            // Insert the post into the database
            wp_insert_post( $my_search_page );
            
        }
    }
}