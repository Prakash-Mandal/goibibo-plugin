<?php

/**
 * @package GoIbiboPLugin
 *
 * Created by PhpStorm.
 * User: prakash
 * Date: 26/3/19
 * Time: 12:57 AM
 */

class GoIbiboPluginDeactivate {

    /**
     * Deactivate Plugin Function
     *
     * @return void
     */
    public static function deactivate() 
    {

        //flush rewrite rules
        flush_rewrite_rules();
    }

}