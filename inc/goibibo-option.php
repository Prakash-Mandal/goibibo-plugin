<?php
/**
 * @package goibibo-plugin
 */
if (! class_exists('GoIbibo_Settings_Menu')) {
    /**
     * Class GoIbibo_Settings_Menu
     */
    class GoIbibo_Settings_Menu
    {
        /**
         * Holds the values to be used in the fields callbacks
         */
        private $options;

        /**
         * Start up
         */
        public function __construct()
        {
            add_action('admin_menu', array($this, 'add_plugin_page'));
            add_action('admin_init', array($this, 'page_init'));
        }

        /**
         * Add options page
         */
        public function add_plugin_page()
        {
            // This page will be under "Settings"
            add_options_page(
                'GoIbibo Settings Admin',
                'GoIbibo Settings',
                'manage_options',
                'goibibo',
                array($this, 'create_goibibo_option_page')
            );
        }

        /**
         * Options page callback
         */
        public function create_goibibo_option_page()
        {
            // Set class property
            $this->options = get_option('goibibo_option_name');
            include 'goibibo-option-page.php';
        }

        /**
         * Register and add settings
         */
        public function page_init()
        {
            register_setting(
                'goibibo_option_group', // Option group
                'goibibo_option_name', // Option name
                array($this, 'sanitize') // Option Callback
            );

            add_settings_section(
                'setting_section_id', // Section ID
                __('GoIbibo Plugin Options', 'textdomain'), // Section Name
                array($this, 'print_section_info'), // Section Callback
                'goibibo' // Page
            );

            add_settings_field(
                'goibibo_api_id', // Feild ID
                __('API ID', 'textdomain'), // Field Label
                array($this, 'goibibo_api_id_callback'), // Field Callback
                'goibibo', // Page
                'setting_section_id' // Section ID
            );

            add_settings_field(
                'goibibo_api_key',
                __('API KEY', 'textdomain'),
                array($this, 'goibibo_api_key_callback'),
                'goibibo',
                'setting_section_id'
            );

            add_settings_field(
                'goibibo_fetch_api_details',
                __('GET API KEY AND API ID', 'textdomain'),
                array($this, 'goibibo_fetch_api_details_callback'),
                'goibibo',
                'setting_section_id'
            );
        }

        /**
         * Sanitize each setting field as needed
         *
         * @param array $input Contains all settings fields as array keys
         *
         * @return array
         */
        public function sanitize($input)
        {
            if (0 === count($input)) {
                return [];
            }
            $new_input = array();
            if (isset($input['goibibo_api_id'])) {
                $new_input['goibibo_api_id'] = sanitize_text_field($input['goibibo_api_id']);
            }
            if (isset($input['goibibo_api_key'])) {
                $new_input['goibibo_api_key'] = sanitize_text_field($input['goibibo_api_key']);
            }
            return $new_input;
        }

        /**
         * @desc Print the Section text
         * @doc Section for getting Input
         */
        public function print_section_info()
        {
            print __('Enter your API KEY and API ID below:', 'textdomain');
        }

        /**
         * Get the settings option array and print one of its values
         *
         * @desc Print the Field for API ID Input
         *
         */
        public function goibibo_api_id_callback()
        {
            printf(
                '<input type="text" id="goibibo_api_id" name="goibibo_option_name[goibibo_api_id]" 
            value="%s" placeholder="API ID">',
                isset($this->options['goibibo_api_id']) ? esc_attr($this->options['goibibo_api_id']) : ''
            );
        }

        /**
         * Get the settings option array and print one of its values
         *
         * @desc Print the Field for API KEY Input
         */
        public function goibibo_api_key_callback()
        {
            printf(
                '<input type="text" id="goibibo_api_key" name="goibibo_option_name[goibibo_api_key]"
            value="%s" placeholder="API KEY" style="width: 275px;">',
                (isset($this->options['goibibo_api_key'])) ? esc_attr($this->options['goibibo_api_key']) : ''
            );
        }

        /**
         * @desc Print the Field for GOIBIBO Link
         */
        function goibibo_fetch_api_details_callback()
        {
            echo '<a id="goibibo_fetch_api_details" target="_blank" class="form-group" href="' . GOIBIBO_DEVELOPER_PATH_URL . '">
              <img class="img-thumbnail" src="' . GOIBIBO_PATH_URL . '/images/gocode.png"
                   alt="' . GOIBIBO_PATH_URL . '"images/gocode.png"
                   style="max-width: 250px; max-height: 150px">
          </a>';
        }

    }
}

if( is_admin() ) {
    $my_settings_page = new GoIbibo_Settings_Menu();
}