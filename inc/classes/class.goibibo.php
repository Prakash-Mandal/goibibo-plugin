<?php
/**
 * @package goibibo-plugin
 *
 */

if (! class_exists('GoIbibo')) {

    /**
     * Class GoIbibo
     */
    class GoIbibo {

        /**
         * @var string API ID
         */
        protected $api_id;
        /**
         * @var string API KEY
         */
        protected $api_key;

        /**
         * GoIbibo constructor.
         */
        public function __construct() 
        {
            $this->_set_default_value();
        }

        /**
         * Function to set API KEY and API ID
         */
        private function _set_default_value()
        {
            $options = get_option('goibibo_option_name');
            $this->api_id = $options['goibibo_api_id'];
            $this->api_key = $options['goibibo_api_key'];

        }

        /**
         * my_price
         *
         * @param  mixed $amount
         *
         * @return void
         */
        public static function my_price($amount = 10 )
        {
            setlocale( LC_MONETARY, 'en_IN.UTF-8' );
            return money_format( '%n', floatval( $amount ) );
        }
    }
}
