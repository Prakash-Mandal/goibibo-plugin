<?php
/**
 * @package goibibo-plugin
 *
 */

if (! class_exists('GoIbibo_Flight') ) {
     /**
      * Class for Dispaying Flight Result
      */
    class GoIbibo_Flight extends GoIbibo {

        /**
         * Private vatrable for listing Airport names.
         */
        private $_airports;

        /**
         * Constructor
         */
        function __construct()
        {
            parent::__construct();

            $this->_airports = json_decode(
                file_get_contents(GOIBIBO_PATH_URL.'data/airports.json'),
                true
            );
        }

        /**
         * Getting City with AITIA CODES
         *
         * @param string $ITAIA_CODE
         *
         * @return array
         */
        private function _get_city($ITAIA_CODE='')
        {
            foreach ($this->_airports['data'] as $airport) {
                if ($ITAIA_CODE === $airport['code']) {
                    return $airport;
                }
            }
            return [];
        }




        /**
         * @desc Getting Flight data
         *
         * @param $source
         * @param $destination
         * @param $travel_date
         * @param $arrival_date
         * @param $seating_class
         * @param $adults
         * @param $children
         * @param $infants
         *
         * @return string|void
         */
        public function flight_search_results(
            $source, 
            $destination, 
            $travel_date,
            $seating_class, 
            $adults, 
            $children, 
            $infants
        ) {

            if ( $source === $destination) {
                ?>
                <div class="container-fluid search-results">
                    <h3>Source and Destination cannot be same</h3>
                </div>
                <?php
                return;
            }
            
            $url = 'http://developer.goibibo.com/api/search/?app_id=' .$this->api_id . '&app_key=' . $this->api_key .
                '&format=json&source=' . $source . '&destination=' . $destination . '&dateofdeparture=' .$travel_date .
                '&dateofArrival=&seatingclass=' . $seating_class . '&adults=' . $adults .
                '&children=' . $children . '&infants=' . $infants . '&counter=100';

            $page_url ='https://www.goibibo.com/flights/air-'.
                $source.'-'.$destination.'-'.$travel_date.'--'.$adults.
                '-'.$children.'-'.$infants.'-'.$seating_class.'-D/';

            $headers = array(
                'timeout' => 500,
                'headers' => array(
                    'content-type'  => 'application/json'
                ),
                "cache-control" => "no-cache"
            );

            $src = $this->_get_city($source);
            $dest = $this->_get_city($destination);
            $date = strtotime($travel_date);

            $data = wp_remote_get($url, $headers);
            $json = wp_remote_retrieve_body($data);
            $flight_list = json_decode($json, true);

            $flight_num = count($flight_list['data']['onwardflights']);
            if (null === $flight_list || 0 === $flight_num) {
                return '<h1> No Flights Found</h1>';
            } ?>

            <div class="container-fluid search-results">
                <div class="row container-fluid bg-secondary text-white">
                    <span class="source">
                        <?php echo $src['city'] . ', ' . $src['country']; ?>
                    </span>
                    <img class="bus" 
                    src="<?php echo GOIBIBO_PATH_URL; ?>/images/plane.svg">
                    <span class="destination">
                        <?php echo $dest['city'] . ', ' . $dest['country']; ?>
                    </span>
                    <span>
                        Departs on : <?php echo date('D M d Y', $date); ?>
                    </span>
                </div>
                <?php
                if(0 === count($flight_list['data']['onwardflights'])) {
                    ?>
                    <div class="container row pt-3 pb-3" style="font-size:18px;">
                        <span> No Flights found </span>
                    </div>
                    <?php
                } else {
                    foreach ($flight_list['data']['onwardflights'] as $flight) {
                        ?>
                        <div class="container row pt-3 pb-3" style="font-size:18px;">
                            <div id="flight-icon" class="flight-icon col-1">
                                <img src="<?php echo GOIBIBO_PATH_URL; ?>images/goibibo-flight-icon.png">
                            </div>
                            <div id="Source" class="col-2 pr-2 pl-2">
                            <span class=""><?php echo $flight['origin']; ?><span>
                            <span><b><?php echo $flight['deptime']; ?></b></span>
                            <br>
                            <span><?php
                                $origin = $this->_get_city($flight['origin']);
                                echo $origin['city'] . ',' . $origin['country'];
                                ?></span>
                            </div>
                            <div id="route-plan"
                                 class="col-3 align-content-center pl-5 pr-2"
                                 style="align-contents:center!important;">
                                <span><?php echo $flight['duration']; ?></span><br>
                                <?php
                                for ($i = 0; $i < count($flight['onwardflights']); $i++) {
                                    ?>
                                    <?php
                                    $origin = $this->_get_city($flight['onwardflights'][$i]['origin']);
                                    ?>
                                    <span data-toggle="tooltip"
                                          title="<?php echo $origin['city'] . ',' . $origin['country']; ?>">
                                <?php echo $flight['onwardflights'][$i]['origin']; ?>
                            </span>
                                    <?php
                                } ?>
                            </div>
                            <div id="Destination" class="col-2 pl-2 pr-2">
                            <span class=""><?php echo $destination; ?><span>
                            <span><b><?php echo $flight['arrtime']; ?></b></span>
                            <br>
                            <span><?php
                                $origin = $this->_get_city($destination);
                                echo $origin['city'] . ',' . $origin['country'];
                                ?></span>
                            </div>
                            <div id="Price-Detail" class="col-3">
                                <small>
                                    <span>Base Fare : <?php echo GoIbibo::my_price($flight['fare']['adultbasefare']); ?></span><br>
                                    <span>Taxes : <?php echo GoIbibo::my_price($flight['fare']['grossamount'] - $flight['fare']['adultbasefare']); ?></span><br>
                                    <span>Gross Amount :<?php echo GoIbibo::my_price($flight['fare']['grossamount']); ?></span>
                                </small>
                                <button class="btn "><a href="<?php echo $page_url; ?>"
                                   value="Book Now" >Book Now </a></button>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <?php
        }
    }
}