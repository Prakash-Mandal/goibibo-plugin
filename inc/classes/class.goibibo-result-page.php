<?php
/**
 * @package goibiboplugin
 *
 */

if (! class_exists('GoIbiboResultPage')) {
    /**
     * @desc Result Page class
     */
    class GoIbiboResultPage
    {
        /**
         * @desc Function for Hotel Search
         *
         * @return string
         */
        function hotel_search()
        {

            $hotel_obj = new GoIbibo_Hotel();
            $src=$trvl_date=$arvl_date='';

            foreach ($_POST['widget-goibibo_hotel_widget'] as $post) {
                $src = ucfirst($post["city_name"]);
                $trvl_date = $post["check_in_date"];
                $arvl_date = str_replace('-', '', $post["check_out_date"]);
            }

            // return $src . ' ' . $trvl_date . ' ' . $arvl_date;
            return $hotel_obj->hotel_search_results($src, $trvl_date, $arvl_date);
        }

        /**
         * @desc Function for Flight Search
         *
         * @return string|void
         */
        function flight_search()
        {
            
            $flight_obj = new GoIbibo_Flight();
            $source=$dest=$trvl_date=$arvl_date='';
            $seating_class=$adult=$children=$infants='';

            foreach ($_POST['widget-goibibo_flight_widget'] as $post) {
                $source = $post['source'];
                $dest = $post['destination'];
                $trvl_date = str_replace('-', '', $post["flight_date"]);
                $seating_class = $post['seating_class'];
                $adults = $post['adults'];
                $children = $post['children'];
                $infants = $post['infants'];
            }

            return $flight_obj->flight_search_results(
                $source,
                $dest,
                $trvl_date,
                $seating_class,
                $adults,
                $children,
                $infants
            );
        }

        /**
         * @desc Function for Bus Search
         *
         * @return string
         */
        function bus_search()
        {
            $bus_obj = new GoIbibo_Bus();
            $src=$dest=$trvl_date=$arvl_date='';

            foreach ($_POST['widget-goibibo_bus_widget'] as $post) {
                $src = ucfirst($post["source"]);
                $dest = ucfirst($post["destination"]);
                $trvl_date = $post["bus_travel_date"];
            }

            return $bus_obj->bus_search_results(
                $src, 
                $dest, 
                $trvl_date,
                $arvl_date
            );
        }

        /**
         * @desc Routing for Short-code
         *
         * @return void|string
         */
        public static function goibibo_plugin_shortcode_init()
        {

            if (! shortcode_exists('result_shortcode')) {

                $search = new GoIbiboResultPage();

                if (isset($_POST['hotel-search'])) {
                    add_shortcode('result_shortcode', array($search, 'hotel_search'));
                } else if (isset($_POST['bus-search'])) {
                    $search->bus_search();
                    add_shortcode('result_shortcode', array($search, 'bus_search'));
                } else if (isset($_POST['flight-search'])) {
                    add_shortcode(
                        'result_shortcode', 
                        array($search, 'flight_search')
                    );
                } else {
                    return '<p> The search results will be shown here on any search submission</p>';
                }
            }
        }
    }
}
