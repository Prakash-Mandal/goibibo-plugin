<?php
/**
 *
 * @package goibibo-plugin
 *
 * @author Prakash <Prakasm@mindfiresolutions.com>
 *
 */

if (! class_exists('GoIbibo_Bus')) {
    /**
     * Class GoIbibo_Bus
     */
    class GoIbibo_Bus extends GoIbibo
    {
        /**
         * GoIbibo_Bus constructor.
         */
        function __construct() 
        {
            parent::__construct();

            wp_enqueue_script('goBus_JScript');
        }
        
        /**
         *  Bus Search Results
         * 
         * @param string $src
         * @param string $dest
         * @param string $trvl_date
         * @param string $arvl_date
         *
         * @return string
         */
        public function bus_search_results(
            $src = '', 
            $dest = '', 
            $trvl_date = '', 
            $arvl_date = '' 
        ) {

            if ($src === $dest) {
                ?>
                <div class="container-fluid search-results">
                    <h3>Source and Destination cannot be same</h3>
                </div>
                <?php
                return;
            }

            $url = GOIBIBO_DEVELOPER_PATH_URL . '/api/bus/search/?app_id='. $this->api_id.'&app_key='.$this->api_key.
                '&format=json&source='.$src.'&destination='.$dest . '&dateofdeparture=' .
                str_replace('-', '', $trvl_date) . '&dateofArrival=' .
                str_replace('-', '', $arvl_date);

            $headers = array(
                'headers' => array(
                    'content-type'  => 'application/json'
                ),
                "cache-control" => "no-cache"
            );

            $data = wp_remote_get($url, $headers);
            $json = wp_remote_retrieve_body($data);
            $bus_list = json_decode($json, true);

            $output = '<div class="container-fluid search-results" style="font-size:18px;">';

            $date = strtotime($trvl_date);
            $output .= '
                <div class="row container-fluid bg-secondary text-white">
                    <span class="source">' . $src . '</span>
                    <img class="bus" src="' . GOIBIBO_PATH_URL . '/images/bus.svg">
                    <span class="destination">'. $dest . '</span>
                    <span>Departs on : ' . date('D M d Y', $date) . '</span>
                </div>';
            
            if (strpos($json, 'Error') !== false) {
                return (false !== strpos($json, 'Invalid Arrival Departure Dates') ) ?
                    '<h3>Bus not found on this Date</h3></div>' : '<h3>Bus not found on this route</h3></div>' ;
            }

            // get values

            $bus_list_num = is_array($bus_list['data']['onwardflights']) ? count($bus_list['data']['onwardflights']) : 0;

            if (0 === $bus_list_num ) {
                $output .= 'No records found for ' . $dest . ' from ' . $src . ' on ' . $trvl_date;
                wp_localize_script(
                        'goBus_JScript',
                        'busScript',
                        array( 'bus_list_num' => $bus_list_num )
                );
            } else {

                for ($i = 0; $i < $bus_list_num; $i++) {

                    $bus = $bus_list['data']['onwardflights'][$i];

                    if (0 === $bus['RouteSeatTypeDetail']['list'][0]['SeatsAvailable']) {
                        continue;
                    }

                    $output .= '<div id="bus' . $i . '" class="container-fluid bg-light row ">';

                    $output .= '<input id="skey' . $i . '" type="hidden" value=' . esc_attr($bus['skey']) . '>';
                    $output .= '<input id="src' . $i . '" type="hidden" value=' . esc_attr($bus['src_voyager_id']) . '>';
                    $output .= '<input id="dest' . $i . '" type="hidden" value=' . esc_attr($bus['dest_voyager_id']) . '>';
                    $output .= '<input id="route' . $i . '" type="hidden" value=' . esc_attr($bus['RouteID']) . '>';

                    $output .= '<div class="col-3 bus-boarding-station">';
                    $output .= esc_attr__($bus['DepartureTime']) . ' ';
                    $output .= '<p> 
                    <label for="boarding-station-' . $i . '">' . __('Select Bording station : ', 'text_domain') .
                        '</label><br>';
                    $output .= '<select class="form-control" id="boarding-station-' . $i . '">';

                    if (0 === count($bus['BPPrims']['list'])) {
                        $output .= '<option name ="' . $src . '" value="' . $dest .
                            '" id="' . $src . '">' . ucfirst(esc_attr__($dest)) .
                            ' - ' . esc_attr__($bus['ArrivalTime']) . '</option>';
                    } else {
                        foreach ($bus['BPPrims']['list'] as $BPlace) {
                            $str_time = substr($BPlace['BPTime'], 11, -1);
                            $output .= '<option name ="' . esc_attr__($BPlace['BPName']) .
                                '" value="' . esc_attr($BPlace['BPId']) .
                                '" id="' . esc_attr($BPlace['BPAddress']) . '">' .
                                ucfirst(esc_attr__($BPlace['BPN'])) . ' - ' .
                                $str_time . '</option>';
                        }
                    }
                    $output .= '</select></p></div>';

                    $output .= '<div class="col-3 bus-disembark-station">';
                    $output .= esc_attr__($bus['ArrivalTime']);
                    $output .= '<p><label class="title" for="disembark-station-' . $i . '">' .
                        __('Select Destination station : ', 'text_domain') .
                        '</label><br>';
                    $output .= '<select class="form-control" id="disembark-station-' . $i . '">';

                    if(0 === count($bus['DPPrims']['list'])) {
                        $output .= '<option name ="' . $dest . '" value="' . $dest .
                            '" id="' . $dest . '">' . ucfirst(esc_attr__($dest)) .
                            ' - ' . esc_attr__($bus['ArrivalTime']) . '</option>';
                    } else {
                        foreach ($bus['DPPrims']['list'] as $DPlace) {
                            $str_time = substr($DPlace['DPTime'], 11, -1);
                            $output .= '<option name ="' . esc_attr__($DPlace['DPName']) .
                                '" value="' . esc_attr($DPlace['DPId']) .
                                '" id="' . esc_attr($DPlace['BPAddress']) . '">' .
                                ucfirst(esc_attr__($DPlace['DPLandmark'])) .
                                ' - ' . $str_time . '</option>';
                        }
                    }
                    $output .= '</select></p></div>';
                    $output .= '<div class="col-3 bus-travels text-center">' .
                        esc_attr__($bus['TravelsName']) . '<br> <small>' .
                        esc_attr__($bus['BusType']) . '</small><br>';
                    $output .= '<span class="title bus-duration">' .
                        __('Duration : ', 'text_domain') . esc_attr__($bus['duration']) .
                        '</span>';
                    $rating = ("0" !== esc_attr__($bus['rating'])) ?
                        esc_attr__($bus['rating']) . '/5</div>' : '</div>';
                    $output .= $rating;
                    $output .= '<div class="col-3 bus-seat-selection text-center"><span>' .
                        __('Available Seats :', 'text_domain') . '</span><span>' .
                        $bus['RouteSeatTypeDetail']['list'][0]['SeatsAvailable'] .
                        '</span><br><span class="price"><img class="rupee" 
                    src="' . GOIBIBO_PATH_URL . '/images/inr.svg"> ' . $bus['fare']['totalfare'] .
                        '</span><br>
                    <button id="select-seats-' . $i . '" type="button" 
                    class="collapsed select-seats btn btn-primary" 
                    data-toggle="collapse" data-target="#collapse' . $i .
                        '">' . __('Select Seat', 'text_domain') . '</button>
                    </div>';
                    $output .= '<hr></div>';
                    $output .= '<div class="collapse seats" id="collapse' . $i .
                        '"></div>';
                }

                $output .= '</div>';

                wp_localize_script(
                    'goBus_JScript',
                    'busScript',
                    array(
                        'GOIBIBO_PATH_URL' => GOIBIBO_PATH_URL,
                        'bus_list_num' => $bus_list_num,
                        'travel_date' => str_replace('-', '', $trvl_date)
                    )
                );
            }
            return $output;
        }

        /**
         * get_bus_seat_layout
         *
         * @param
         * @param  mixed $skey
         *
         * @return void
         */
        public function get_bus_seat_layout($i = 0, $skey = '' )
        {
            $seat_url = GOIBIBO_DEVELOPER_PATH_URL . '/api/bus/seatmap/?app_id=' .
                $this->api_id . '&app_key=' . $this->api_key .
                '&format=json&skey=' . $skey;
            $headers = array(
                'headers' => array(
                    'content-type'  => 'application/json'
                ),
                "cache-control" => "no-cache"
            );

            $data = wp_remote_get($seat_url, $headers);
            $json = wp_remote_retrieve_body($data);
            $seat_list = json_decode($json, true);
            $max_depth = 0;

            $output = '<div class="row select-seat">'.
                '<div class="container-fluid col-6 pl-3 seat-layout" >';
            foreach ( $seat_list['data']['onwardSeats'] as $value) {
                $img_col = $value['ColumnNo'] * 25 ;
                $img_row = $value['RowNo'] *25 ;
                
                if ($img_row > $max_depth) {
                    $max_depth = $img_row;
                }

                $isBooked = ( "bookedSeat" === $value['seat_status'] || "bookedSleeper" === $value['seat_status'] );
                
                $seat_class = ( "Seater" === $value['SeatType'] ) ? 'bus-seat' : 'bus-sleeper-seat';
                
                $image = ( "Seater" === $value['SeatType'] ) ?
                ( ( $isBooked ) ?
                    GOIBIBO_PATH_URL . 'images/bus_seat_booked_icon_goibibo.png' :
                    GOIBIBO_PATH_URL . 'images/bus_seat_icon_goibibo.png') :
                ( ( $isBooked ) ?
                    GOIBIBO_PATH_URL . 'images/bus_sleeper_seat_booked_icon_goibibo.png' :
                    GOIBIBO_PATH_URL . 'images/bus_sleeper_seat_icon_goibibo.png');

                $output .= '<div id="seat' . $value['SeatName'] . 
                    '" class="' . $seat_class . '" style="padding:0pt; top: ' . $img_row .
                    'px; left: ' . $img_col . 'px; position: absolute"' .
                    'data-fare="' . $value['BaseFare'] . '">';
                $output .= '<img id="' . $value['SeatName'] . '" class="seat" '.
                    'src="' . $image . '" data-toggle="tooltip" title="';
                $output .= ( $isBooked ) ? ('Booked,Seat number ' . $value['SeatName']) :
                    'Seat number ' . $value['SeatName'] . ' Fare Rs. ' . $value['BaseFare'];
                $output .= '" >';
                $output .= '</div>';

            }

            $output .= '</div>';

            $output .= '<div class="container col-6 select-value" style="height: ' . 
                ($max_depth+50). 'px;" ><span>' . 
                __( 'Selected Seat', 'text-domain') . ': </span>'.
                '<span id="selected_seat' . $i .'"></span><br>';
            $output .= '<span>Total Price : <img class="rupee" src="' . 
                GOIBIBO_PATH_URL . 'images/inr.svg">'.
                '<span id="selected_price' . $i . '"></span></span><br>'.
                '<span class="message"></span><br>'.
                '<input id="book-btn-' . $i . '" class="btn" 
                    type="button" value="Book Seat"></div>';

            $output .= '</div>';

            echo $output;
            wp_exit();
        }
    }
}
