<?php
/**
 * @package goibiboplugin
 *
 */


if (! class_exists('GoIbibo_Hotel')) {
    /**
     * GoIbibo Hotel class
     */
    class GoIbibo_Hotel extends GoIbibo
    {
        /**
         * Stores hotel ids
         */
        private $_hotel_list;
        private $_hotelIDs;
        
        /**
         * Constructor
         */
        function __construct()
        {
            $this->_hotel_list = '';
            $this->_hotelIDs = '';
            parent::__construct();
        }

        /**
         * _get_hotel_id_list
         *
         * @param  mixed $src
         * @param  mixed $trvl_date
         * @param  mixed $arvl_date
         *
         * @return void|string|array|mixed
         */
        private function _get_hotel_id_list($src, $trvl_date, $arvl_date)
        {
            $city_hotels_url = 'https://developer.goibibo.com/api/cyclone/?app_id=' .
                $this->api_id . '&app_key=' . $this->api_key . '&city_id=' . $src .
                '&check_in=' . str_replace('-', '', $trvl_date ) . '&check_out=' . $arvl_date;

            $headers = array(
                'timeout'     => 500,
                'headers'       => array(
                    'content-type' => 'application/json'
                ),
                "cache-control" => "no-cache"
            );

            $data       = wp_remote_get( $city_hotels_url, $headers );
            $json       = wp_remote_retrieve_body( $data );
            $hotel_list = json_decode( $json, true );

            $output = '';

            if (strpos($json, 'Error') !== false) {
                $output .= 'Message : ' . esc_attr__($hotel_list['Error']) . '<div>';
                return $output;
            }

            $hotel_list_num = is_array($hotel_list['data']) ?
                count($hotel_list['data']) : 0;

            $hotel_ids = '';
            if (! is_array($hotel_list['data']) && 0 !== $hotel_list_num) {
                $output .= '<div> <h3> Sorry No Hotels Found !!</h3></div> </div>';
                return $output;
            } else {
                $i = 0;
                foreach ($hotel_list['data'] as $hotel_id => $value) {
                    $hotel_ids .= $hotel_id . ',';
                    if (24 === $i++) {
                        break;
                    }
                }
            }

            $hotel_ids = rtrim($hotel_ids, ', ');

            return $hotel_ids;
        }

        /**
         * Hotel Search Results
         *
         * @param  mixed $src
         * @param  mixed $trvl_date
         * @param  mixed $arvl_date
         *
         * @return void|string|mixed
         */
        public function hotel_search_results($src, $trvl_date, $arvl_date)
        {
            $hotel_ids = $this->_get_hotel_id_list($src, $trvl_date, $arvl_date);

            $url = 'https://developer.goibibo.com/api/voyager/?app_id=' . $this->api_id .
                '&app_key=' . $this->api_key . '&method=hotels.get_hotels_data' .
                '&id_list=[' . $hotel_ids . ']&id_type=_id';

            $headers = array(
                'timeout' => 500,
                'headers'       => array(
                    'content-type' => 'application/json'
                ),
                "cache-control" => "no-cache"
            );

            $data = wp_remote_get($url, $headers);
            $json = wp_remote_retrieve_body($data);
            $hotel_detail = json_decode($json, true);

            if (! is_array($hotel_detail['data']) || 0 === count($hotel_detail['data'])) {
                $output = '<div> <h3> Sorry No Hotels Found !!!</h3></div> </div>';
                return $output;
            }

            $this->_hotel_list = $hotel_detail;

            $i = 0;
            $this->_hotel_list['data'] = [];
            $name = $id = $hotel_page_url = $desc = $image = $area = $address = '';
            $rating = $site = $price = $disc_price = '';

            foreach ($hotel_detail['data'] as $id => $value) {

                if (isset( $value['hotel_geo_node']['name'] ) ) {
                    $name = $value['hotel_geo_node']['name'] ;
                }
                if ( isset( $value['hotel_data_node']['extra']['gir_data']['seo_reviews'][0]['hotelCity'] ) ) {
                    $city = $value['hotel_data_node']['extra']['gir_data']['seo_reviews'][0]['hotelCity'] ;
                }
                $hotel_page_url = 'https://www.goibibo.com/gostays/'.
                    str_replace(' ', '-', strtolower($name)). '-hotel-in-' .
                    strtolower($city) . '-' . $id;
                $desc = $value['hotel_data_node']['desc']['default'];

                if ( isset( $value['hotel_data_node']['img_selected']['thumb']['wpl'] ) ) {
                    $image =  $value['hotel_data_node']['img_selected']['thumb']['wpl'] ;
                } else {
                    $image = GOIBIBO_PATH_URL . 'images/hotel_icon_goibibo.png';
                }
                
                $area = '';
                if ( isset( $value['hotel_data_node']['extra']['location_slug'] ) ) {
                    $area = $value['hotel_data_node']['extra']['location_slug'] ;
                }
                
                if ( isset( $value['hotel_data_node']['extra']['gir_data']['seo_reviews'][0]['full'] ) ) {
                    $address = __( $value['hotel_data_node']['extra']['gir_data']['seo_reviews'][0]['full'] );
                } else {
                    $address = __( $value['hotel_data_node']['loc']['full'] );
                }
                $address = ltrim( $address, "NA , " );
                
                if (0 < $value['hotel_data_node']['extra']['gir_data']['hotel_rating'] ) {
                    $rating = __( $value['hotel_data_node']['extra']['gir_data']['hotel_rating'] );
                } elseif (0 < $value['hotel_data_node']['rating'] ) {
                    $rating = __( $value['hotel_data_node']['rating'] );
                } else {
                    $rating = __( $value['hotel_data_node']['extra']['ta_data']['hotel_rating'] );
                }

                if ( isset ($value['hotel_data_node']['contact']['web'][0] ) ) {
                    $url = 'http://' . $value['hotel_data_node']['contact']['web'][0] ;
                }
                $site = curl_init($url) ?
                    'http://' . $value['hotel_data_node']['contact']['web'][0] :
                    'https://' . $value['hotel_data_node']['contact']['web'][0] ;
                if ( isset( $url ) ) {
                    $site = $hotel_page_url;
                }

                $price = $hotel_detail["data"][$id]["op"] ;
                if (! $price) {
                    $price = $value['hotel_data_node']['stats']['dyn_prc']['web_101']['p'] ;
                }
                $disc_price =  $hotel_detail["data"][$id]["mp"] ;
                if (! $disc_price) {
                    $disc_price =  $value['hotel_data_node']['stats']['dyn_prc']['web_101']['dp'] ;
                }

                $hotel = [
                    "name" => $name,
                    "id" => $id,
                    "hotel_page_url" => $hotel_page_url,
                    // "desc" => $desc,
                    "image" => $image,
                    "area" => $area,
                    "address" => $address,
                    "rating" => $rating,
                    "hotel_site" => $site,
                    "max_price" => $price,
                    "min_price" => $disc_price
                ];

                array_push($this->_hotel_list['data'], $hotel);
            }

            $output = '<div class="row container-fluid bg-secondary text-white">
                    <span class="source">'. $city . '</span>
                    <img class="bus" 
                    src="' . GOIBIBO_PATH_URL . '/images/hotel_icon_goibibo.png">
                    <span>' . date('D M d Y', $trvl_date) . '</span>
                </div>';

            return $output . $this->show_hotel_list( $this->_hotel_list['data'] );

        }

        /**
         * Filter by Hotel Rating
         * 
         * @return void
        */
        public function filter_by_hotel_rating($minRating = 0, $maxRating = 5 )
        {
            $myFilterList['data'] = [];
                       
            foreach ($this->_hotel_list['data'] as $key => $hotel) {
                if ($minRating >= $hotel['rating'] && $maxRating <= $hotel['rating']) {
                    $myFilterList['data'][$key] = $hotel;
                }
            }

            if (0 === count($myFilterList['data'])) {
                return '<div> <h3> Sorry No Hotels Found !!!</h3></div> </div>';
            } else {
                return $this->__hotel_list( $this->_hotel_list['data'] );
            }

        }

        /**
            return $this->show_hotel_list( $this->_hotel_list['data'] );
         * Display Hotel List
         *
         * @param  mixed $hotel_detail
         *
         * @return void|string
         */
        public function show_hotel_list($hotel_detail )
        {
            $min_price = 100000;
            $max_price = 0;

            $output = '<div class="jumbotron-fluid container-fluid search-results">'.
                        '<div class="row">';
            $output .= '<div class="col">';

            foreach ( $hotel_detail as $key => $hotel) {
                $output .= '
                <div id="' . $hotel['id'] . '" class="hotel-details card">
                    <div class="card-body row">
                        <div class="center col-4">
                            <a href="' . $hotel['hotel_page_url'] . '">
                                <img class="hotel-image" src = "' . $hotel['image'] . '" alt = "' . $hotel['image'] . '">
                            </a>
                        </div>
                        <div class="col-8 pt-4">
                            <p>
                            <a href="' . $hotel['site']. '">' . $hotel['name'] . '</a><br>
                                <small>';
                $output .= ($hotel['area']) ? 
                    'Area : ' . $hotel['area'] : 'Address : ' . $hotel['address'];
                $output .= '</small></p>';
                $output .= '<div style="display: flex;">
                                <p>
                                    Rating :
                                </p>
                                <div >';
                for ( $x = 0; $x < 5; $x++ ) {
                    if ( floor($hotel['rating'])-$x >= 1 ) { 
                        $output .= '<i class="fa fa-star"></i>'; 
                    } elseif ( $hotel['rating']-$x > 0 ) { 
                        $output .= '<i class="fa fa-star-half-o"></i>'; 
                    } else { 
                        $output .= '<i class="fa fa-star-o"></i>'; 
                    }
                }
                $output .= '</div></div>';
                            
                $output .= '<p> Price : ' . GoIbibo::my_price( $hotel['max_price'] );
                $output .= ($hotel['max_price'] === $hotel['min_price']) ? 
                    '' : ' - ' . GoIbibo::my_price( $hotel['min_price'] );
                $output .= '<button class="btn btn primary" style="float:right;" ><a class=" book-hotel" target="_blank" href="' .
                                $hotel['hotel_page_url'].'">Book Now</a></button></p>';
                $output .= '</div></div>';
                $output .= '</div>';

                $max_price = ( $max_price < $hotel['max_price'] ) ? 
                    $hotel['max_price'] : $max_price ;
                $min_price = ( $min_price > $hotel['min_price'] ) ? 
                        $hotel['min_price'] : $min_price ;

            }

            $output .= '</div></div>';

            if (100000 === $min_price) {
                $min_price = 100;
            }
            if (0 === $max_price) {
                $max_price = 100000;
            }

            wp_localize_script(
                'goHotel_JScript',
                'pricescript',
                array(
                    'min_price' =>  $min_price,
                    'max_price' => $max_price
                )
            );

            return $output;
        }
    }
}