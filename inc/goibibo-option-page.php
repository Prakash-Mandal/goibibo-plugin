<?php
/**
 * @package goibibo-plugin
 */
?>
<?php settings_errors('goibibo_option_name'); ?>
<div class="wrap">
<h1><?php esc_html(get_admin_page_title()); ?></h1>

    <form style="max-width: 50%" action="options.php" method="post">
        <?php
        // output security fields for the registered setting "wporg_options"
        settings_fields( 'goibibo-option-group' );

        // output setting sections and their fields
        // (sections are registered for "goibibo", each field is registered to a specific section)
        do_settings_sections( 'goibibo' );

        // output save settings button
        submit_button( 'Save Settings' );
        ?>

    </form>
</div>