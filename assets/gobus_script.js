
var $ = jQuery.noConflict();
var skey;
var i=0;

jQuery(document).on('click', '.select-seats', function(e){

    i = $(this).data('target').substr('9');
    skey = $('#skey'+i).val();

    $('.collapse.show').removeClass('show');

    if ($('#collapse'+i).html()){
        return;
    }

    let data = {
        'i' : i,
        'skey' : skey,
        action : 'SeatLayout'
    };
    jQuery.post(seatscript.ajax_url, data, function(response){
        $('#collapse'+i).html(response);
    });

    e.preventDefault();
});

var selectedSeats = [];
var source = [];
var destination = [];
var route = [];
var bus_source = [];
var bus_dest = [];
var total_price = [];
$(document).ready(function(e){
    for(let i=0; i < busScript.bus_list_num; i++) {
        selectedSeats[i] = [];
        total_price[i]=0;
        $('#collapse'+i).on('click', '.bus-seat .seat', function(e) {
            let id = $(this).attr('id');
            let title = $(this).attr('title');
            if (title.includes('Booked')) {
                return;
            }
            let fare = title.split(' ');
            fare = parseInt(fare[fare.length-1]);
            if (! selectedSeats[i].includes(id) && ! title.includes('Booked')) {
                selectedSeats[i].push(id);
                if (6 < selectedSeats[i].length) {
                    selectedSeats[i].pop();
                    $('span.message').show();
                    $('span.message').html('Cannot book more than 6 Seats');
                    $('#book-btn-'+i).prop("disabled", true);
                    return;
                } else {
                    $('span.message').html('');
                    $('#book-btn-'+i).prop("disabled", false);
                }
                let src = busScript.GOIBIBO_PATH_URL+'images/bus_selected_seat_icon_goibibo.png';
                $(this).attr('src', src);
                total_price[i]=total_price[i]+fare;
            } else {
                $('span.message').html('');
                selectedSeats[i].splice(selectedSeats[i].indexOf(id),1);
                let src = busScript.GOIBIBO_PATH_URL+'images/bus_seat_icon_goibibo.png';
                $(this).attr('src', src);
                total_price[i]=total_price[i]-fare;
            }
            $('#selected_seat'+i).html('');
            selectedSeats[i].forEach(function (item) {
                let seats = $('#selected_seat'+i).html();
                $('#selected_seat'+i).html(seats+item+',');
            });
            let seats = $('#selected_seat'+i).html();
            seats = seats.substr(0,seats.length-1);
            $('#selected_seat'+i).html(seats);
            $('#selected_price'+i).html(total_price[i]);
        });
        $('#collapse'+i).on('click', '.bus-sleeper-seat .seat', function(e) {
            let id = $(this).attr('id');
            let title = $(this).attr('title');
            if (title.includes('Booked')) {
                return;
            }
            let fare = title.split(' ');
            fare = parseInt(fare[fare.length-1]);
            if (! selectedSeats[i].includes(id) && ! title.includes('Booked')) {
                selectedSeats[i].push(id);
                if (6 < selectedSeats[i].length) {
                    selectedSeats[i].pop();
                    $('span.message').show();
                    $('span.message').html('Cannot book more than 6 Seats');
                    $('#book-btn-'+i).prop("disabled", true);
                    return;
                } else {
                    $('span.message').html('');
                    $('#book-btn-'+i).prop("disabled", false);
                }
                let src = busScript.GOIBIBO_PATH_URL+'images/bus_selected_sleeper_seat_icon_goibibo.png';
                $(this).attr('src', src);
                total_price[i]=total_price[i]+fare;
            } else {
                selectedSeats[i].splice(selectedSeats[i].indexOf(id),1);
                let src = busScript.GOIBIBO_PATH_URL+'images/bus_sleeper_seat_icon_goibibo.png';
                $(this).attr('src', src);
                total_price[i]=total_price[i]-fare;
            }
            $('#selected_seat'+i).html('');
            selectedSeats[i].forEach(function (item) {
                let seats = $('#selected_seat'+i).html();
                $('#selected_seat'+i).html(seats+item+',');
            });
            let seats = $('#selected_seat'+i).html();
            seats = seats.substr(0,seats.length-1);
            $('#selected_seat'+i).html(seats);
            $('#selected_price'+i).html(total_price[i]);
        });
        $('#collapse'+i).on('click', '#book-btn-'+i, function(e) {
            if( 0 == selectedSeats[i].length) {
                return;
            }
            
            var bookURL = "https://www.goibibo.com/bus-booking/?booking-key=bus-";
            bookURL += $('.source').html()+'-';
            bookURL += $('.destination').html()+'-';
            bookURL += busScript.travel_date + '--' + selectedSeats[i].length+'-0-0-';
            bookURL += $('#src'+i).val() + '-' + $('#dest'+i).val() + '-' + $('#route'+i).val();
            
            source[i] = $('#src'+i).val();
            destination[i] = $('#dest'+i).val();
            route[i] = $('#route'+i).val();
            bus_source[i] = $('#boarding-station-'+i).val();
            bus_dest[i] = $('#disembark-station-'+i).val();

            bookURL += '-' + $('#boarding-station-'+i).val() + '-';
            selectedSeats[i].forEach(function(item){
                bookURL += item + '-';
            });
            // window.location.href = bookURL;
            window.open(bookURL, '_blank', 'location=yes,height=640,width=1000,scrollbars=yes,status=yes');

        });
    }    
});
