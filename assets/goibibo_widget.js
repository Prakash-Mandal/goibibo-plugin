$ = jQuery.noConflict();

$(document).ready(function(e) {
    $('input.check-in').change(function () {
        var numberOfDaysToAdd = 1;
        
        signin = new Date($(this).val());
        signout = new Date(signin.getTime() + numberOfDaysToAdd * 24 * 60 * 60 * 1000);
        
        day = ("0" + signout.getDate()).slice(-2);
        month = ("0" + (signout.getMonth() + 1)).slice(-2);

        newdate = signout.getFullYear() + '-' + month + '-' + day;
                
        $('input.check-out').val(newdate);
        $('input.form-control.check-out').attr('min',$(this).val());
    });
});