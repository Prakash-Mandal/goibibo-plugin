<?php
/**
 *
 * @package GoIbiboPLugin
 *
 * Trigger this file on Plugin Uninstall
 *
 * delete CPT
 * delete all the plugin data from the DB.
 *
 *
 * Created by PhpStorm.
 * User: prakash
 * Date: 25/3/19
 * Time: 6:13 PM
 * 
 * @return void
 *
 */

function uninstall() {
    echo '<script> alert("Deleting Data"); </script>';

    if (! defined('WP_UNINSTALL_PLUGIN')) {
        echo 'Deleting Data';
    } else {
        echo "Cannot Delete Data, Unauthorised User";
        die;
    }

    global $wpdb;
    global $table_prefix;

    $delete_table = 'DROP TABLE IF EXISTS '.$table_prefix.'cities';
    $wpdb->query($delete_table);
    $delete_option = 'DELETE FROM '.$table_prefix.'options 
        WHERE option_name = goibibo_option_name';
    $wpdb->query($delete_option);

}
