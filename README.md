## GoIbibo Plugin in WordPress
### Description:- 
This would be  WordPress plugin which would search for the hotels, flights and buses in a city. The search result would be presented in a detailed View for a better selection option. And After the selection of the desired service user would be taken to the original site for booking of the hotel, buse or flight. 

### Functionality :-
1. Get list of hotels in a particular city : This plugin would be provided with a search box widget where the user can give input the name of the city where it wants to search for the hotels. The search box will be equipped with autocomplete feature with places names. The page will be then provided with a list of hotels in the desired city and a map widget will also present to show the location of the hotels with price labels and names, clicking on the hotel card, the user would be redirected to the actual site where he can book the room.
* Search for a city hotel list
* Get list of hotels
* Select a hotel


 ``` 
 NOTE : The Travel widget will provide you with input boxes for From, Destination and Travel Date for Flights/Bus (select using option)  search. On selecting the Flight option the user will be asked for Seating Class and Passengers list (No. of Adults, Children and infants). The user will be provided with a list of selection and on selecting the bus will be provided with selection of seat. After the selection of seat(s) user will be redirected to the goibibo page. 
 ```

2. Get buses to a city : The Bus search widget will ask for the destination and source of the user with the travel date. Then the user will be provided with a list of buses to the destined city. On selecting a bus, will be provided with the available seats and on selecting the seats and the boarding station and clicking the Book button, he will be redirected to the GoIbibo payment portal.. 
* Enter source and destination
* Enter Date of travel
* Select Bus and Seat No.
* Select Boarding Station and Destination Station


3. Get Flights to a city : The Flight search Widget will need the source, destination, Date of Travel, Travel Class and Travellers types (Adults, Children or infants). And then would show the list of upcoming fights to the destined city.
* Enter source and destination
* Enter Date of travel
* Enter Seating Class
* Enter No. of Passengers
* Select from the list of Flights

### API’s being used :
 1. Returns hotels search result : ```https://developer.goibibo.com/api/voyager/get_hotels_by_cityid/```
    * App id
    * App key
    * City id
 2. Returns bus search results : ```https://developer.goibibo.com/api/bus/search/```
    * App id
    * App key
    * Format
    * Source
    * Destination
    * Date of Departure
    * Date of Arrival (optional)
 3. Returns flight search results : ```https://developer.goibibo.com/api/search/```
    * App id 
    * App key
    * Format
    * Source
    * Destination
    * Date of Departure
    * Date of Arrival (optional)
    * Seating Class
    * Adults
    * Children
    * Infants
    * Flight Type (100: Domestic , 0: International)


### Platform :-
Web Application on WordPress 

### Developer - 
 ``` Prakash Kumar Mandal ```
### Goals :-
* Provide with search list for flights, buses and hotels
* Search list of hotels with
* Most rated hotel
* Categories of hotels
  * Budget
  * Premium
* Nearest to a prefered location
* Search list for Flights and Buses
  * Lowest Fare First
  * Minimum Travel Time

### Technologies to be used:- 
1. WordPress (Framework)
2. PHP (Back-end)
3. HTML & CSS (Front-end)
4. jQuery

### Limitations :-
 ##### *Cannot Book hotels, can only select hotels*
 ##### *Do not have any Payment Gateway*


# Milestone Breakups Plan with Gantt Chart :- 

``` https://drive.google.com/open?id=1eAx5wllXpA4EnNuHAwQg06-dbJr1a0fX ```
